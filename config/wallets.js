(function() {
  var AppHelper, Wallet, currency, exports, fs, options, path, walletType, wallets, _i, _len, _ref;

  fs = require("fs");

  AppHelper = require("../lib/app_helper");

  wallets = {};

  _ref = AppHelper.getCurrencies(true);
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    currency = _ref[_i];
    walletType = currency.toLowerCase();
    options = GLOBAL.appConfig().wallets[walletType];
    path = "" + (process.cwd()) + "/lib/crypto_wallets/" + walletType + "_wallet.js";
    if (fs.existsSync(path)) {
      Wallet = require(path);
    } else {
      Wallet = require("" + (process.cwd()) + "/lib/crypto_wallet");
    }
    wallets[walletType] = new Wallet(options);
  }

  exports = module.exports = wallets;

}).call(this);
