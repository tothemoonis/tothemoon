async              = require "async"
GLOBAL.appConfig   = require "../config/config"
environment        = process.env.NODE_ENV or "development"
require "../config/logger"  if environment is "production"
GLOBAL.db          = require "../server/database"
TransactionsHelper = require "../lib/transactions_helper"
GLOBAL.wallets     = require "../config/wallets"
CronJob            = require("cron").CronJob

new CronJob
  cronTime: "*/50 * * * * *"
  start: true
  onTick: ()->
    if not cronInProgress
      cronInProgress = true
      TransactionsHelper.processedUserIds = []
      GLOBAL.db.getPendingWithdrawals (err, withdrawals)->
        async.mapSeries withdrawals, TransactionsHelper.processWithdrawal, (err, result)->
          console.log err  if err
          console.log "#{result} - #{new Date()}"
          cronInProgress = false

console.log "Withdrawals cron started"