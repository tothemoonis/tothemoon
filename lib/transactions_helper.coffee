AppHelper = require "./app_helper"

WITHDRAWAL_FEE = 10000 # satoshis

TransactionsHelper =
  
  processedUserIds: []

  processWithdrawal: (withdrawal, callback)->
    return callback null, "#{withdrawal.id} - user already had a processed withdrawal"  if TransactionsHelper.processedUserIds.indexOf(withdrawal.user_id) > -1
    btcAmount = (withdrawal.amount * -1 - WITHDRAWAL_FEE) / 100000000
    GLOBAL.wallets["btc"].sendToAddress withdrawal.bitcoin_withdrawal_address, btcAmount, (err, response = "")=>
      console.error "Could not withdraw to #{withdrawal.bitcoin_withdrawal_address} #{btcAmount} BTC", err  if err
      return callback null, "#{withdrawal.id} - not processed - #{err}"  if err
      console.log "Sent #{btcAmount} BTC to #{withdrawal.bitcoin_withdrawal_address} for #{withdrawal.id} on #{new Date()}", response
      TransactionsHelper.processedUserIds.push withdrawal.user_id
      GLOBAL.db.setFundingsWithdrawalTxid withdrawal.id, response, (err)->
        callback err, "#{withdrawal.id} - processed"

  loadDeposit: (transaction, callback)->
    return callback()  if transaction.category isnt "receive"
    if not GLOBAL.wallets["btc"].isBalanceConfirmed transaction.confirmations
      console.log "Not enough confirmations #{transaction.confirmations} - #{transaction.txid}"
      return callback()
    GLOBAL.db.getUserByDepositAddress transaction.address, (err, user)->
      return callback()  if not user
      GLOBAL.db.getDepositByUserIdAndTxid user.id, transaction.txid, (err, existentTransaction)->
        return callback()  if existentTransaction
        satoshisAmount = parseInt transaction.amount * 100000000
        GLOBAL.db.makeDeposit user.id, satoshisAmount, transaction.txid, (err, tx)->
          return callback null, "Could not load deposit with txid #{transaction.txid} for #{user.id}", err  if err
          console.log "Loaded #{transaction.amount} BTC from #{transaction.txid} for #{user.id} on #{new Date()}"
          callback null, user.id


exports = module.exports = TransactionsHelper