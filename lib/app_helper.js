(function() {
  var AppHelper, CURRENCIES, exports;

  CURRENCIES = ["btc"];

  AppHelper = {
    getCurrencies: function() {
      return CURRENCIES;
    }
  };

  exports = module.exports = AppHelper;

}).call(this);
