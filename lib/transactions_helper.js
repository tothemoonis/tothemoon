(function() {
  var AppHelper, TransactionsHelper, WITHDRAWAL_FEE, exports;

  AppHelper = require("./app_helper");

  WITHDRAWAL_FEE = 10000;

  TransactionsHelper = {
    processedUserIds: [],
    processWithdrawal: function(withdrawal, callback) {
      var btcAmount;
      if (TransactionsHelper.processedUserIds.indexOf(withdrawal.user_id) > -1) {
        return callback(null, "" + withdrawal.id + " - user already had a processed withdrawal");
      }
      btcAmount = (withdrawal.amount * -1 - WITHDRAWAL_FEE) / 100000000;
      return GLOBAL.wallets["btc"].sendToAddress(withdrawal.bitcoin_withdrawal_address, btcAmount, (function(_this) {
        return function(err, response) {
          if (response == null) {
            response = "";
          }
          if (err) {
            console.error("Could not withdraw to " + withdrawal.bitcoin_withdrawal_address + " " + btcAmount + " BTC", err);
          }
          if (err) {
            return callback(null, "" + withdrawal.id + " - not processed - " + err);
          }
          console.log("Sent " + btcAmount + " BTC to " + withdrawal.bitcoin_withdrawal_address + " for " + withdrawal.id + " on " + (new Date()), response);
          TransactionsHelper.processedUserIds.push(withdrawal.user_id);
          return GLOBAL.db.setFundingsWithdrawalTxid(withdrawal.id, response, function(err) {
            return callback(err, "" + withdrawal.id + " - processed");
          });
        };
      })(this));
    },
    loadDeposit: function(transaction, callback) {
      if (transaction.category !== "receive") {
        return callback();
      }
      if (!GLOBAL.wallets["btc"].isBalanceConfirmed(transaction.confirmations)) {
        console.log("Not enough confirmations " + transaction.confirmations + " - " + transaction.txid);
        return callback();
      }
      return GLOBAL.db.getUserByDepositAddress(transaction.address, function(err, user) {
        if (!user) {
          return callback();
        }
        return GLOBAL.db.getDepositByUserIdAndTxid(user.id, transaction.txid, function(err, existentTransaction) {
          var satoshisAmount;
          if (existentTransaction) {
            return callback();
          }
          satoshisAmount = parseInt(transaction.amount * 100000000);
          return GLOBAL.db.makeDeposit(user.id, satoshisAmount, transaction.txid, function(err, tx) {
            if (err) {
              return callback(null, "Could not load deposit with txid " + transaction.txid + " for " + user.id, err);
            }
            console.log("Loaded " + transaction.amount + " BTC from " + transaction.txid + " for " + user.id + " on " + (new Date()));
            return callback(null, user.id);
          });
        });
      });
    }
  };

  exports = module.exports = TransactionsHelper;

}).call(this);
