var assert = require('assert');

var db = require('./database');

// Doesn't validate
module.exports = function(userId, satoshis, withdrawalAddress, callback) {
    assert(typeof userId === 'number');
    assert(satoshis > 10000);
    assert(typeof withdrawalAddress === 'string');
    assert(typeof callback === 'function');


    db.makeWithdrawal(userId, satoshis, withdrawalAddress, function(err, fundingId) {
        if (err) {
            if (err.code === '23514')
                callback('NOT_ENOUGH_MONEY');
            else
                callback(err);
            return;
        }

        callback(null);
    });
};

